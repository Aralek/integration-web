import React from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route
} from "react-router-dom";

import {
    FRUITS_ROUTE,
    VEGETABLES_ROUTE
} from './config/routes';

import FruitsPage from './containers/Fruit';
import VegetablesPage from './containers/Vegetable';


function App() {
    return (
        <Router>
            <Switch>
                <Route path={VEGETABLES_ROUTE} component={VegetablesPage} />
                <Route path={FRUITS_ROUTE} component={FruitsPage} />
            </Switch>
        </Router>
    );
}


export default App;
