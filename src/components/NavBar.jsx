import React from 'react';

import './NavBar.scss';

import Basket from "../images/basket.PNG";
import Rabbit from "../images/rabbit.PNG";
import Search from "../images/search.PNG";

const NavBar = ({animationClass}) => {

    let menuOpen = false;

    return (
        <nav className={"navbar " + animationClass}>
            <div className="has-margin-bottom">
                <img alt="Logo" className="logo" src={Rabbit} />
            </div>
            <div>
                <img alt="Basket" className="basket-icon" src={Basket} />
            </div>
            <div className="separator" />
            <div className="navbar-button"
                 onClick={() => {
                     if(menuOpen) {
                         document.querySelector('.nav-menu-container').classList.remove('animation-menu-background-left-right');
                         document.querySelector('.nav-menu').classList.remove('animation-fade-in');
                         document.querySelector('.tomatoes').classList.remove('animation-tomatoes-apparition');
                     }
                     else{
                         document.querySelector('.nav-menu-container').classList.add('animation-menu-background-left-right');
                         document.querySelector('.nav-menu').classList.add('animation-fade-in');
                         document.querySelector('.tomatoes').classList.add('animation-tomatoes-apparition');
                     }
                     menuOpen = !menuOpen;
                 }}
            />
            <div className="separator" />
            <div>
                <img alt="Search" className="search-icon" src={Search} />
            </div>
            <p className="has-margin-bottom brand"><span className="secondary-yellow">ORGA</span><span
                className="secondary-green">BITS</span></p>
        </nav>
    )
}

export default NavBar;
