import React from 'react';
import "./NavMenu.scss"
import ProductsList from "./ProductsList";
import Tomatoes from "../images/tomatoes.png";

const NavMenu = () => {

    const fruits = ["Pineapples", "Strawberries", "Apples", "Citrus"];
    const vegetables = ["Cauliflowers", "Tomatoes", "Eggplants", "Carrots"];

    return (
        <div className="nav-menu-container">
            <div className="main nav-menu">
                <div className="block product-grid">
                    <div className="product-title">
                        <div className="bullet"/>
                        <span className="menu-title">Our fruits</span>
                    </div>
                    <ProductsList
                        products={fruits}
                    />
                </div>
                <div className="block product-grid">
                    <div className="product-title">
                        <div className="bullet"/>
                        <span className="menu-title">Our vegetables</span>
                    </div>
                    <ProductsList
                        products={vegetables}
                    />
                </div>
                <div className="block">
                    <div className="bullet"/>
                    <span className="menu-title">Our vision</span>
                </div>
                <div>
                    <div className="bullet"/>
                    <span className="menu-title">Contact us</span>
                </div>
            </div>
            <img alt="Tomatoes" className="tomatoes" src={Tomatoes}/>
        </div>
    )
}

export default NavMenu;
