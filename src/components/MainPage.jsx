import React from 'react';
import NavBar from "./NavBar";

import './MainPage.scss';

import Facebook from "../images/facebook.png";
import Instagram from "../images/instagram.png";
import Twitter from "../images/twitter.png";
import Youtube from "../images/youtube.png";
import {Link} from "react-router-dom";
import ProductsList from "./ProductsList";
import NavMenu from "./NavMenu";

const MainPage = ({background, icon, page, nextRoute}) => {

    const fruits = ["Pineapples", "Strawberries", "Apples", "Citrus"];
    const vegetables = ["Cauliflowers", "Tomatoes", "Eggplants", "Carrots"];

    return (
        <div>
            <div className="loader">
                <p className="loader">loading...</p>
            </div>
            <div className="container">
                <NavBar
                    animationClass={"animation-fade-in animation-at-start"}
                />
                <div className="main-main-container">
                    <NavMenu />
                    <div className={"background " + background} />
                    <div className="main-container">
                        <div className="main">
                            <div className="products-list">
                                <div className="animation-left-right-30 animation-at-start">
                                    <div className="bullet" />
                                    <span className="menu-title">Our products</span>
                                </div>
                                <ProductsList
                                    animationClass={"animation-left-right-60 animation-at-start"}
                                    products={fruits}
                                />
                                <ProductsList
                                    animationClass={"animation-left-right-90 animation-at-start"}
                                    products={vegetables}
                                />
                                <div className="products-list-column animation-right-left-5 animation-at-start">
                                    <img alt="twitter" className="network-icon" src={Twitter} />
                                    <img alt="youtube" className="network-icon" src={Youtube} />
                                    <img alt="instagram" className="network-icon" src={Instagram} />
                                    <img alt="facebook" className="network-icon" src={Facebook} />
                                </div>
                            </div>

                            <div className="footer">
                                <p className="pages animation-bottom-up-10 animation-at-start">
                                    <span className="actual-page secondary-yellow">{page}</span>
                                    <span className="secondary-yellow"> / </span>
                                    <span className="total-pages secondary-green">2</span>
                                </p>
                                <Link to={nextRoute} className="next-page animation-right-left-20 animation-at-start">
                                    <div>
                                        <img alt="" className="footer-icon" src={icon} />
                                    </div>
                                    <div className="footer-arrow yellow-to-green"><span>▶</span></div>
                                </Link>
                            </div>
                        </div>
                    </div>
                    <div className="title-container animation-fade-in animation-at-start">
                        <p className="title primary-yellow"><span className="transparency-65">Our fruits</span><br />
                            <span className="transparency-65">& vegetables</span><br />
                            <span className="transparency-65">are </span><span className="primary-green">organic.</span>
                        </p>
                        <div className="description">
                            <p className="transparency-100">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean
                                commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis
                                parturient montes, nascetur ridiculus mus.</p>
                        </div>
                        <div className="discover-button yellow-to-green">
                            DISCOVER ▶
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default MainPage;
