import React from 'react';
import "./ProductsList.scss"

const ProductsList = ({products, animationClass}) => {
    return (
        <div className={"products-list-column " + animationClass}>
            {products.map(product => (
                <div key={product} className="product">
                    <div className="bullet small-bullet" />
                    <span>{product}</span>
                </div>
            ))}
        </div>
    )
}

export default ProductsList;
