import React from 'react';
import MainPage from "../components/MainPage";
import IconCauliflower from "../images/icon-cauliflower.png";
import {VEGETABLES_ROUTE} from "../config/routes";

const FruitsPage = () => {
    return (
        <MainPage
            background={"bg-pineapple"}
            icon={IconCauliflower}
            page={"1"}
            nextRoute={VEGETABLES_ROUTE}
        />
    )
}

export default FruitsPage;
