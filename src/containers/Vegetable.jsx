import React from 'react';
import MainPage from "../components/MainPage";
import IconPineapple from "../images/icon-pineapple.png";
import {FRUITS_ROUTE} from "../config/routes";

const VegetablesPage = () => {
    return (
        <MainPage
            background={"bg-cauliflower"}
            icon={IconPineapple}
            page={"2"}
            nextRoute={FRUITS_ROUTE}
        />
    )
}

export default VegetablesPage;
